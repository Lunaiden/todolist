# Todo-list
Exercice de création d'une application web de todo-list.

Backend en Go avec base de données NoSQL (MongoDB)

## Installation
Si vous n'avez pas Go installé sur votre machine, vous pouvez le télécharger ici : [golang.org](https://golang.org/dl/).


## Lancement et arrêt de l'application
Dans un terminal depuis le dossier **/todolist**, taper : `go run main.go`

Vous pouvez ouvrir l'application depuis http://localhost:8080/

Pour arrêter l'application, depuis le terminal, appuyez sur `Ctrl + C`

## Ajouter les fichiers

- [ ] [Créer](https://gitlab.com/-/experiment/new_project_readme_content:2a113432a8a0b0a6f7c65a333c493985?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) ou [télécharger](https://gitlab.com/-/experiment/new_project_readme_content:2a113432a8a0b0a6f7c65a333c493985?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) les fichiers
- [ ] [Ajouter les fichiers en ligne de commande](https://gitlab.com/-/experiment/new_project_readme_content:2a113432a8a0b0a6f7c65a333c493985?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) ou pousser un répertoire Git existant avec la commande suivante :

```
cd existing_repo
git remote add origin https://gitlab.com/Lunaiden/todolist.git
git branch -M main
git push -uf origin main
```