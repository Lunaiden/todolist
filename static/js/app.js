// Récupération des tâches depuis l'API en Go + création du tableau pour l'affichage
const tasksTable = document.getElementById("table_body");
const addTaskForm = document.getElementById("addForm");


// Affichage du formulaire d'ajout au click sur la ligne "+ Ajouter une tâche"
$("#addFormLine").hide();
$("#addLine").on("click", function () {
    const $addForm = $("#addFormLine");
    $(this).hide();
    if ($addForm.not(":visible")) {
        $($addForm).show();
    }
})

// Fonction d'affichage des tâches dans le tableau
const renderList = (tasks) => {
    let output = '';
    tasks.forEach(todo => {
        // Formattage de la deadline pour + de lisibilité
        const deadline = new Date(todo.deadline)
        const dateNow = new Date();
        let formattedDate =
            ("0" + deadline.getDate()).slice(-2) + "/" +
            ("0" + (deadline.getMonth() + 1)).slice(-2) + "/" +
            deadline.getFullYear();

        // Vérification de la deadline de deadline
        if(deadline < dateNow && todo.state !== 3) {
            updateState(3, todo._id);
        }

        // définition du nom de l'état selon l'int récupéré
        let state;
        let stateClass;
        switch (todo.state) {
            case 0:
                state = "À faire";
                stateClass = "toDo"
                break;
            case 1:
                state = "En cours";
                stateClass = "inProgress"
                break;
            case 2:
                state = "Fait";
                stateClass = "done"
                break;
            case 3:
                state = "En retard";
                stateClass = "late"
                break;
        }

        // ajout du HTML
        output += `
                <tr>
                    <td class="round ${stateClass}"><i class="fas fa-circle"></i></td>
                    <td class="title text-start">${todo.title}</td>
                    <td class="deadline">${formattedDate}</td>
                    <td id="stateCell" class="state ${stateClass}">${state}</td>
                    <td class=""><a data-id=${todo._id}><svg id="edit" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
  <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
</svg></a> </td>
<td class=""><a data-id=${todo._id}><i id="delete" class="fas fa-times"></i></a></td>
                </tr>
                `;
        tasksTable.innerHTML = output;
    })
}

const todoUrl = "http://localhost:8080/todo"

// Récupérer - Afficher les tâches
// Method: GET
const getTasks = () => {
    fetch(todoUrl, {method: "GET"})
        .then(res => res.json())
        .then(data => renderList(data))

}
getTasks();
setInterval(getTasks, 5000);

const titleValue = document.getElementById("title");
const deadlineValue = document.getElementById("deadline");
const stateValue = 0;
const done = false;

// Créer - Insérer une nouvelle tâche
// Method: POST
addTaskForm.addEventListener('submit', (e) => {
    // évite le rechargement de la page
    e.preventDefault();

    const newTask =
        JSON.stringify({
            title: titleValue.value,
            deadline: deadlineValue.value,
            done: done,
            state: stateValue
        });

    fetch(todoUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: newTask
    })
        .then(res => res.json())
        .then(() => getTasks())

    // cache à nouveau le formulaire de création et on réaffiche la ligne "+ ajouter une tâche"
    $("#addFormLine").hide();
    $("#addLine").show();
})

tasksTable.addEventListener('click', (e) => {
    e.preventDefault();
    let isDeleteButton = e.target.id === 'delete';
    let isEditButton = e.target.id === 'edit';
    let id = e.target.parentElement.dataset.id;

    // Supprimer une tâche
    // Method: DELETE
    if (isDeleteButton) {
        fetch("http://localhost:8080/deleteTask/" + id, {
            method: 'DELETE',
        })
            .then(res => res.json())
            .then(() => getTasks())
    }

    if (isEditButton) {
        const editCellForm = `
        <form id="editForm" method="post">
                        <td><select class="form-select" name="state" id="state-select">
                       <option value="0">À faire</option> 
                       <option value="1">En cours</option> 
                       <option value="2">Fait</option> 
                       <option value="3">En retard</option> 
                            </select>
                        <input id="update_submit" class="btn btn-dark" type="submit" value="Modifier"></td>

                    </form>
        `
        const parent = e.target.parentElement;
        const grandParent = parent.parentElement;
        const tr = grandParent.parentElement;
        const stateCell = tr.querySelector('.state');

        stateCell.className = "";
        stateCell.innerHTML = editCellForm;
        const updateSubmit = document.getElementById('update_submit');
        let newState = document.getElementById("state-select");

        updateSubmit.addEventListener("click", (e) => {
            e.preventDefault();

            updateState(parseInt(newState.value), id);
        })
    }
})

// Mise à jour de la tâche
// Method: PATCH
const updateState = (newState, id) => {
    let newValue =
        JSON.stringify({
            state: newState,
        })

    fetch("http://localhost:8080/updateTask/" + id, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: newValue,
    })
        .then(res => res.json())
        .then(() => getTasks())
}

// Barre de recherche


