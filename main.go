package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"net/http"
	"time"
)

// Création du struct Task
type Task struct {
	ID       primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
	Title    string             `bson:"title,omitempty" json:"title,omitempty"`
	Done     bool               `bson:"done" json:"done"`
	Deadline string             `bson:"deadline,omitempty" json:"deadline,omitempty"`
	State    int                `bson:"state" json:"state"`
}

var uri = "mongodb+srv://todoUser:pwdForTodoUser@todo.dpltf.mongodb.net/todo?retryWrites=true&w=majority"

// Fonction de création d'une nouvelle tâche
func createTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")
	var t Task
	json.NewDecoder(r.Body).Decode(&t)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	tasksCollection := client.Database("Todo").Collection("todo")
	taskResult, err := tasksCollection.InsertOne(ctx, t)
	if err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(w).Encode(taskResult)
}

// Récupère toutes les tâches en BDD
func tasksList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var tasks []Task
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	tasksCollection := client.Database("Todo").Collection("todo")
	cursor, err := tasksCollection.Find(ctx, bson.M{})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	if err = cursor.All(ctx, &tasks); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(tasks)
}

// Récupère une seule tâche selon son ID
func getTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var task Task
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	tasksCollection := client.Database("Todo").Collection("todo")
	err := tasksCollection.FindOne(ctx, Task{ID: id}).Decode(&task)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(task)
}

// Modifie la tâche correspondant à l'ID récupéré dans l'URL
func updateTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var t Task
	json.NewDecoder(r.Body).Decode(&t)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	tasksCollection := client.Database("Todo").Collection("todo")
	taskResult, err := tasksCollection.UpdateOne(
		ctx, bson.M{"_id": id},
		bson.D{
			{"$set", t},
		})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(taskResult)
}

// Supprime la tâche correspondant à l'ID récupéré dans l'URL
func deleteTask(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, _ := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	tasksCollection := client.Database("Todo").Collection("todo")
	taskResult, err := tasksCollection.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(taskResult)
}

func main() {
	println("Starting the app...")
	// Connection Cluster MongoDB Atlas
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected and pinged.")
	fmt.Println("Vous pouvez accéder à l'url http://localhost:8080/")

	// Routes
	router := mux.NewRouter()
	router.HandleFunc("/todo", tasksList).Methods("GET")
	router.HandleFunc("/todo", createTask).Methods("POST")
	router.HandleFunc("/todo/{id}", getTask).Methods("GET")
	router.HandleFunc("/updateTask/{id}", updateTask).Methods("PUT")
	router.HandleFunc("/deleteTask/{id}", deleteTask).Methods("DELETE")

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static")))

	//http.Handle("/", http.FileServer(http.Dir("./static")))
	if err := http.ListenAndServe(":8080", router); err != nil {
		log.Fatal(err)
	}
}
